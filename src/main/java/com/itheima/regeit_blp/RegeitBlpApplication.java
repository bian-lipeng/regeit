package com.itheima.regeit_blp;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Slf4j
@SpringBootApplication
@ServletComponentScan  //扫描filter
@EnableCaching  //启用springcache
@EnableTransactionManagement
public class RegeitBlpApplication {

    public static void main(String[] args) {
        SpringApplication.run(RegeitBlpApplication.class, args);
        log.info("项目启动");
    }
}
