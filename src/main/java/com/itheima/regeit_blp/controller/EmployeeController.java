package com.itheima.regeit_blp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.regeit_blp.common.BaseContext;
import com.itheima.regeit_blp.common.R;
import com.itheima.regeit_blp.pojo.Employee;
import com.itheima.regeit_blp.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;

@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    //登录
    //设置rest风格请求路径
    @PostMapping("/login")
    public R<Employee> login(HttpServletRequest httpServletRequest, @RequestBody Employee employee) {
        //拿到请求的json数据
        //判断用户姓名是否存在
        QueryWrapper<Employee> employeeQueryWrapper = new QueryWrapper<>();
        employeeQueryWrapper.eq("username", employee.getUsername());
        Employee one = employeeService.getOne(employeeQueryWrapper);
        if (one == null) {
            return R.error("用户名不存在");
        } //存在的话对密码进行md5加密和数据库的密码进行比较
        String s = DigestUtils.md5DigestAsHex(employee.getPassword().getBytes(StandardCharsets.UTF_8));

        if (one.getPassword().equals(s)) {
            if (one.getStatus() == 1) {
                httpServletRequest.getSession().setMaxInactiveInterval(10000);
                httpServletRequest.getSession().setAttribute("employee", one.getId());

                return R.success(one);
            } else {
                return R.error("账号已被禁用");
            }
        } else {
            return R.error("密码错误");
        }


    }


    //注销
    @PostMapping("/logout")
    public R outLogin(HttpServletRequest request) {
        //清楚session数据
        request.getSession().removeAttribute("employee");
        return R.success("退出成功");

    }


//页面响应参数
    //http://localhost:8080/employee/page?page=1&pageSize=10
    // http://localhost:8080/employee/page?page=1&pageSize=10&name=%E8%BE%B9%E7%AB%8B%E9%B9%8

@RequestMapping("/page")
    public R page(Integer page,Integer pageSize,String name){
    QueryWrapper<Employee> employeeQueryWrapper = new QueryWrapper<>();

    IPage<Employee> pages =new Page<>();
    //分页器设置
    pages.setCurrent(page).setSize(pageSize);
    //根据条件查询
    employeeQueryWrapper.like(StringUtils.isNotEmpty(name),"name",name);

    //排序
     employeeQueryWrapper.orderByDesc("update_time");
   employeeService.page(pages, employeeQueryWrapper);


    return R.success(pages);
}


    //新增员工
    @PostMapping
    public R<String> save(HttpServletRequest request, @RequestBody Employee employee) {
        //接受请求的参数
        //数据补全
        log.info("新增员工，员工信息：{}", employee.toString());
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes(StandardCharsets.UTF_8)));
      //  employee.setCreateTime(LocalDateTime.now());
      //  employee.setUpdateTime(LocalDateTime.now());
       // Long user = (Long) request.getSession().getAttribute("employee");

      //  employee.setCreateUser(user);

       // employee.setUpdateUser(user);


            employeeService.save(employee);

        return R.success("新增员工成功");
    }

/*    http://localhost:8080/employee
    请求说明请求方式PUT请求路径/employee
    请求参数{"id":xxx,"status":xxx}*/
    //禁用和修改员工
    @PutMapping
    public R update(HttpServletRequest request ,@RequestBody Employee employee){
        //根据session获取修改人信息
        if (employee.getId()==1&& employee.getStatus()==0){
            return R.error("不能禁用管理员");
        }

     //   Long employee1 = (Long)request.getSession().getAttribute("employee");
     //   employee.setUpdateUser(employee1);
        //更新修改时间
      //  employee.setUpdateTime(LocalDateTime.now());
       employeeService.updateById(employee);

        return R.success("禁用成功");
    }


    //修改员工数据回显
    //GET http://localhost:8080/employee/1454699587213312002
    @GetMapping("/{id}")
    public R update (@PathVariable Long id){

        //根据id查询
        QueryWrapper<Employee> employeeQueryWrapper = new QueryWrapper<>();
        employeeQueryWrapper.eq("id",id);

        Employee one = employeeService.getOne(employeeQueryWrapper);
        return R.success(one);
    }



}
