package com.itheima.regeit_blp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.regeit_blp.common.R;
import com.itheima.regeit_blp.dto.DishDto;
import com.itheima.regeit_blp.pojo.Category;
import com.itheima.regeit_blp.pojo.Dish;
import com.itheima.regeit_blp.pojo.DishFlavor;
import com.itheima.regeit_blp.pojo.Setmeal;
import com.itheima.regeit_blp.service.CategoryService;
import com.itheima.regeit_blp.service.DishFlavorService;
import com.itheima.regeit_blp.service.DishService;
import com.itheima.regeit_blp.service.impl.DishServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dish")
@RestController
public class DishController {
   @Autowired
   private DishService dishService;
  @Autowired
  private CategoryService categoryService;
  @Autowired
  private DishFlavorService dishFlavorService;

  // GET http://localhost:8080/dish/page?page=1&pageSize=10&name=%E5%8C%85%E5%90%AB
     //页面响应数据
    @GetMapping("/page")
    public R page(Integer page ,Integer pageSize ,String name){

        IPage<Dish> pages=new Page<>(page,pageSize);
        QueryWrapper<Dish> objectQueryWrapper = new QueryWrapper<>();
        if (name!=null){
            objectQueryWrapper.like("name",name);
        }

        objectQueryWrapper.orderByAsc("price");
        dishService.page(pages,objectQueryWrapper);
          List<Dish> records = pages.getRecords();
        List<Dish> dishDtos = new ArrayList<>();
        for (Dish record : records) {
           Long categoryId = record.getCategoryId();
            Category byId = categoryService.getById(categoryId);
            String name1 = byId.getName();
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(record,dishDto);
            dishDto.setCategoryName(name1);
           dishDtos.add(dishDto);
 }
        pages.setRecords(dishDtos);
        return R.success(pages);
    }

    //post http://localhost:8080/dish
    //添加菜品 难点 设计多表事务

    @PostMapping
    @CacheEvict(value = "dish",allEntries = true)
    public R add(@RequestBody DishDto dishDto){
          dishService.adddishdto(dishDto);
        return R.success("新增菜品成功");

    }

    //Get  http://localhost:8080/dish/1413385247889891330
    //根据菜品id回显菜品数据
    @GetMapping("/{id}")
    public R display(@PathVariable  Long id){
        Dish byId = dishService.getById(id);
        QueryWrapper<DishFlavor> dishFlavorQueryWrapper = new QueryWrapper<>();
        dishFlavorQueryWrapper.eq("dish_id",id);
        List<DishFlavor> list = dishFlavorService.list(dishFlavorQueryWrapper);

        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(byId,dishDto);
        dishDto.setFlavors(list);
        return R.success(dishDto);

    }

      //修改菜品
    @PutMapping
    @CacheEvict(value = "dish",allEntries = true)
    public R update ( @RequestBody DishDto dishDto){
        R r = dishService.updateAll(dishDto);
        return r;
    }


/*管理端请求
http://localhost:8080/dish/list?categoryId=1397844263642378242
    Request Method: GET*/

    //用户端请求  http://localhost:8080/dish/list?categoryId=1397844263642378242&status=1
    //新建套餐时候展示菜品信息  && 用户端显示口味和菜品信息
    @GetMapping("/list")
    //此注解查询缓存中有无数据 有数据把缓存的数据作为方法的返回值返回  无数据把方法的返回值加入缓存
  @Cacheable(value = "dish" ,key ="#categoryId" )
   public R list (Long categoryId){
        List<Dish> list = dishService.list(new QueryWrapper<Dish>().eq(categoryId != null, "category_id", categoryId).eq("status", 1));
        ArrayList<Dish> dishes = new ArrayList<>();
        for (Dish dish : list) {
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(dish,dishDto);

           List<DishFlavor> dish_id = dishFlavorService.list(new QueryWrapper<DishFlavor>().eq("dish_id", dish.getId()));
          dishDto.setFlavors(dish_id);
             dishes.add(dishDto);
        }

        return R.success(dishes);
    }

    //菜品删除  需求如果起售则不进行删除
   //delete  http://localhost:8080/dish?ids=1413385247889891330,1413384757047271425
  @DeleteMapping
  @CacheEvict(value = "dish",allEntries = true)
    public R deleteAll(Long[] ids){
       R r= dishService.delete(ids);
        return r;

  }

  //批量停售
   //post http://localhost:8080/dish/status/0?ids=1413385247889891330
  //批量停售
  @PostMapping("/status/{status}")
  @CacheEvict(value = "dish",allEntries = true)
  public R update(@PathVariable("status")Integer status, Long...ids){
      for (Long id : ids) {
          Dish byId = dishService.getById(id);
          byId.setStatus(status);
          dishService.updateById(byId);
      }

      return R.success("状态信息更改成功");
  }

}
