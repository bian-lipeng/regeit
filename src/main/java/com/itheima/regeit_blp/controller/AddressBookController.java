package com.itheima.regeit_blp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.itheima.regeit_blp.common.R;
import com.itheima.regeit_blp.pojo.AddressBook;
import com.itheima.regeit_blp.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/addressBook")
public class AddressBookController {

@Autowired
private AddressBookService addressBookService;
 //get   http://localhost:8080/addressBook/list
    //获取地址信息集合
    @GetMapping("/list")
    public R list(HttpServletRequest httpServletRequest){

        Long user = (Long)httpServletRequest.getSession().getAttribute("user");

        List<AddressBook> list = addressBookService.list(new QueryWrapper<AddressBook>().eq("user_id",user));
         return R.success(list);
    }


   //修改默认地址 http://localhost:8080/addressBook/default

    @PutMapping("/default")
    public R deault(@RequestBody AddressBook addressBook){
        addressBookService.update(new UpdateWrapper<AddressBook>().eq("is_default",1).set("is_default",0));
        addressBookService.update(new UpdateWrapper<AddressBook>().eq("id",addressBook.getId()).set("is_default",1));
        return R.success("修改成功");
    }


 //修改数据回显   http://localhost:8080/addressBook/1417414926166769666
    @GetMapping("/{id}")
    public R display(@PathVariable Long id){
        AddressBook byId = addressBookService.getById(id);
       return R.success(byId);
    }


    //修改收货地址
    @PutMapping
    public R update(@RequestBody AddressBook addressBook){

        addressBookService.updateById(addressBook);
        return R.success("修改成功");
    }

    //添加 post请求 对userid进行信息补齐 采用自动补齐策略
    @PostMapping
    public R add(@RequestBody AddressBook addressBook, HttpServletRequest httpServletRequest){
        Long user = (Long)httpServletRequest.getSession().getAttribute("user");
        addressBook.setUserId(user);
        addressBookService.save(addressBook);
        return R.success("添加成功");

    }



    //http://localhost:8080/addressBook?ids=1456868385802485761
    @DeleteMapping
    public R delete(Long ids){

        addressBookService.removeById(ids);
        return R.success("删除成功");
    }

    //查找默认地址
    @GetMapping("/default")
    public R deaultFind(HttpServletRequest httpServletRequest){
        QueryWrapper<AddressBook> addressBookQueryWrapper = new QueryWrapper<>();
        addressBookQueryWrapper.eq("user_id",httpServletRequest.getSession().getAttribute("user"));
         addressBookQueryWrapper.eq("is_default",1);
        AddressBook one = addressBookService.getOne(addressBookQueryWrapper);
        return R.success(one);
    }
}
