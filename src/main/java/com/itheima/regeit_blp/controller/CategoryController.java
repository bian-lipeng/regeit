package com.itheima.regeit_blp.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.regeit_blp.common.R;
import com.itheima.regeit_blp.pojo.Category;
import com.itheima.regeit_blp.pojo.Dish;
import com.itheima.regeit_blp.pojo.Setmeal;
import com.itheima.regeit_blp.service.CategoryService;
import com.itheima.regeit_blp.service.DishService;
import com.itheima.regeit_blp.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.management.Query;
import java.util.List;

@RestController
//设置一级路径
@RequestMapping("/category")
@Slf4j
public class CategoryController {

    @Autowired
    private CategoryService service;
    @Autowired
    private DishService dishService;
    @Autowired
    private SetmealService setmealService;

    //查询所有
//GET http:localhost:8080/category/page?page=1&pageSize=10
@GetMapping("/page")
    public R page(Integer page,Integer pageSize){
    IPage<Category> pages=new Page();
    QueryWrapper query=new QueryWrapper();
    query.orderByAsc("sort");
    service.page(pages,query);
   return R.success(pages);

}


//POST http://localhost:8080/category
//添加功能
    @PostMapping
    public  R addDishes(@RequestBody Category category){
          service.save(category);
    return R.success("添加成功");
    }
  //删除
    //DELETE http://localhost:8080/category?id=1397844263642378200

    @DeleteMapping
    public R delete(Long id){
        Category byId = service.getById(id);
        Integer type = byId.getType();
        if (type==1){
            QueryWrapper<Dish> dishQueryWrapper = new QueryWrapper<>();
            dishQueryWrapper.eq("category_id",id);
            List<Dish> list = dishService.list(dishQueryWrapper);
            if (!list.isEmpty()){
                return R.error("该菜品分类下含有菜品不能删除");
            }
        }else{
            QueryWrapper<Setmeal> setmealQueryWrapper = new QueryWrapper<>();
            setmealQueryWrapper.eq("category_id",id);
             List<Setmeal> list = setmealService.list(setmealQueryWrapper);
            if (!list.isEmpty()){
                return R.error("该套餐分类下含有菜品不能删除");
            }

        }


        service.removeById(id);
      return R.success("删除成功");
    }


  //  PUT http://localhost:8080/category
    //修改数据
    @PutMapping
public R update(@RequestBody  Category category){
    service.updateById(category);
    return R.success("数据更新成功");

}



     //新建菜品 菜品信息回显
    //http://localhost:8080/category/list?type=1
    @GetMapping("/list")
    public R list(Integer type){
      //type 1表示当前是菜品 查询所有菜品对应的菜品分类
        QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();
        categoryQueryWrapper.eq(type!=null,"type",type);
        List<Category> list = service.list(categoryQueryWrapper);

        return R.success(list);
    }
}
