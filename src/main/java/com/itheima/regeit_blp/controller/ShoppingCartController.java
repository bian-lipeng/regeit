package com.itheima.regeit_blp.controller;

import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.itheima.regeit_blp.common.R;
import com.itheima.regeit_blp.pojo.ShoppingCart;
import com.itheima.regeit_blp.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
  //    /shoppingCart/list
    @Autowired
  private  ShoppingCartService shoppingCartService;

    @GetMapping("/list")
    public R list(HttpServletRequest httpServletRequest){
        QueryWrapper<ShoppingCart> shoppingCartQueryWrapper = new QueryWrapper<>();
        shoppingCartQueryWrapper.eq("user_id",httpServletRequest.getSession().getAttribute("user"));
        List<ShoppingCart> list = shoppingCartService.list(shoppingCartQueryWrapper);

        return  R.success(list);
    }

//添加购物车

@PostMapping("/add")
    public R add(@RequestBody ShoppingCart shoppingCart,HttpServletRequest httpServletRequest){
    QueryWrapper<ShoppingCart> shoppingCartQueryWrapper = new QueryWrapper<>();
    Long user = (Long) httpServletRequest.getSession().getAttribute("user");
    shoppingCartQueryWrapper.eq("user_id",user);
        if (shoppingCart.getDishId()!=null){
            shoppingCartQueryWrapper.eq("dish_id",shoppingCart.getDishId());
        }else{
            shoppingCartQueryWrapper.eq("setmeal_id",shoppingCart.getSetmealId());
        }
    ShoppingCart one = shoppingCartService.getOne(shoppingCartQueryWrapper);

    if (one!=null){
        one.setNumber(one.getNumber()+1);
        shoppingCartService.updateById(one);
        return R.success(one);
    }else{
        shoppingCart.setUserId(user);
        shoppingCart.setNumber(1);
        shoppingCartService.save(shoppingCart);
        return R.success(shoppingCart);
    }


}
   // http://localhost:8080/shoppingCart/clean
@DeleteMapping("/clean")
    public R clean (HttpServletRequest httpServletRequest){
    Long user = (Long) httpServletRequest.getSession().getAttribute("user");
    shoppingCartService.remove(new QueryWrapper<ShoppingCart>().eq("user_id",user));
    return R.success("成功");

}

@PostMapping("/sub")
    public R sub(@RequestBody ShoppingCart shoppingCart,HttpServletRequest httpServletRequest){
    QueryWrapper<ShoppingCart> shoppingCartUpdateWrapper = new QueryWrapper<>();
    Long user = (Long) httpServletRequest.getSession().getAttribute("user");
    shoppingCartUpdateWrapper.eq("user_id",user);
    QueryWrapper<ShoppingCart> QueryWrapper = new QueryWrapper<>();
    if (shoppingCart.getDishId()!=null){
           shoppingCartUpdateWrapper.eq("dish_id",shoppingCart.getDishId());
     }else{
        shoppingCartUpdateWrapper.eq("setmeal_id",shoppingCart.getSetmealId());

    }
    ShoppingCart one = shoppingCartService.getOne(shoppingCartUpdateWrapper);
    if (one.getNumber()==1){
        shoppingCartService.removeById(one);
        return R.success("删除成功");
    }
    one.setNumber(one.getNumber()-1);
    shoppingCartService.updateById(one);
    return R.success(one);


}
}
