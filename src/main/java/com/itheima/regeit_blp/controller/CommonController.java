package com.itheima.regeit_blp.controller;

import com.itheima.regeit_blp.common.R;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/common")
public class CommonController {

@Value("${path.blp}")
private String name;

    @PostMapping("/upload")
    public R upload(MultipartFile file){
          //获取源文件名称
        String originalFilename = file.getOriginalFilename();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        UUID uuid = UUID.randomUUID();
        try {
            file.transferTo(new File(name +uuid+suffix));
        } catch (IOException e) {
            e.printStackTrace();
        }


        return R.success(uuid+suffix);
    }


    //http://localhost:8080/common/download?name=%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20210816220525.jpg

    @GetMapping("/download")
    public R download(@RequestParam("name") String id, HttpServletResponse httpServletResponse) throws IOException {

        FileInputStream fileInputStream = new FileInputStream(new File(name, id));
        ServletOutputStream outputStream = httpServletResponse.getOutputStream();
        byte []arr=new byte[1024];
        int len;
        while ((len=fileInputStream.read(arr))!=-1){
            outputStream.write(arr,0,len);
            outputStream.flush();
        }
        fileInputStream.close();
        return R.success("完成");

    }
}
