package com.itheima.regeit_blp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.regeit_blp.common.BaseContext;
import com.itheima.regeit_blp.common.R;

import com.itheima.regeit_blp.dto.OrdersDto;
import com.itheima.regeit_blp.pojo.AddressBook;
import com.itheima.regeit_blp.pojo.OrderDetail;
import com.itheima.regeit_blp.pojo.Orders;
import com.itheima.regeit_blp.service.AddressBookService;
import com.itheima.regeit_blp.service.OrderDetailService;
import com.itheima.regeit_blp.service.OrdersService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/order")
public class OrdersController {


    @Autowired
   private OrdersService ordersService;
   // http://localhost:8080/order/submit
    //提交订单

    @Autowired
    private OrderDetailService orderDetailService;

    @Autowired
    private AddressBookService addressBookService;

    @PostMapping("/submit")
    public R submit(@RequestBody Orders orders){
       ordersService.add(orders);
        return R.success("提交成功");
    }

    //历史订单
    @GetMapping("/userPage")
public R Userpage (Integer page, Integer pageSize, HttpServletRequest httpServletRequest){
        Page<Orders> ordersPage = new Page<>(page,pageSize);
        QueryWrapper<Orders> ordersQueryWrapper = new QueryWrapper<>();
        ordersQueryWrapper.eq("user_id",httpServletRequest.getSession().getAttribute("user"));
        ordersQueryWrapper.orderByDesc("order_time");
        ordersService.page(ordersPage,ordersQueryWrapper);
        List<Orders> records = ordersPage.getRecords();
        List<Orders> orders = new ArrayList<>();
        for (Orders record : records) {
            OrdersDto ordersDto = new OrdersDto();
            BeanUtils.copyProperties(record,ordersDto);
            QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
            orderDetailQueryWrapper.eq("order_id",record.getId());
            List<OrderDetail> list = orderDetailService.list(orderDetailQueryWrapper);
            ordersDto.setOrderDetails(list);
            Long addressBookId = record.getAddressBookId();
            AddressBook byId = addressBookService.getById(addressBookId);
            ordersDto.setPhone(byId.getPhone());
            ordersDto.setUserName(byId.getConsignee());
            ordersDto.setConsignee(byId.getConsignee());
            ordersDto.setAddress(byId.getDetail());
            orders.add(ordersDto);
        }
        ordersPage.setRecords(orders);
        return R.success(ordersPage);
    }

    //管理端订单
    //beginTime=2021-11-12%2000%3A00%3A00&endTime=2021-12-12%2023%3A59%3A59
    @GetMapping("/page")
    public R page (Integer page, Integer pageSize, String number, String beginTime,String endTime){

        QueryWrapper<Orders> ordersQueryWrapper = new QueryWrapper<>();
        ordersQueryWrapper.between(beginTime!=null,"order_time",beginTime,endTime);
        ordersQueryWrapper.orderByAsc("order_time");
        ordersQueryWrapper.like(number!=null,"number",number);
        Page<Orders> ordersPage = new Page<>(page,pageSize);

        ordersService.page(ordersPage,ordersQueryWrapper);
        List<Orders> records = ordersPage.getRecords();
        List<Orders> orders = new ArrayList<>();
        for (Orders record : records) {
            OrdersDto ordersDto = new OrdersDto();
            BeanUtils.copyProperties(record,ordersDto);
            QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
            orderDetailQueryWrapper.eq("order_id",record.getId());
            List<OrderDetail> list = orderDetailService.list(orderDetailQueryWrapper);
            ordersDto.setOrderDetails(list);
            Long addressBookId = record.getAddressBookId();
            AddressBook byId = addressBookService.getById(addressBookId);
            ordersDto.setPhone(byId.getPhone());
            ordersDto.setUserName(byId.getConsignee());
            ordersDto.setConsignee(byId.getConsignee());
            ordersDto.setAddress(byId.getDetail());
            orders.add(ordersDto);
        }
        ordersPage.setRecords(orders);
        return R.success(ordersPage);
    }


}
