package com.itheima.regeit_blp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.regeit_blp.common.R;
import com.itheima.regeit_blp.pojo.User;
import com.itheima.regeit_blp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import java.util.Map;
import java.util.Random;

@RestController
@RequestMapping("/user")
public class UserController {

  @Autowired
    private UserService userService;

   //post   http://localhost:8080/user/sendMsg
   //发送验证码请求
    @PostMapping("/sendMsg")
    public R sendmsg(@RequestBody User user, HttpServletRequest httpServletRequest){
        Random random = new Random();
        String yz=""+random.nextInt(10)+random.nextInt(10)+random.nextInt(10)+random.nextInt(10);

        System.out.println(yz);
        httpServletRequest.getSession().setAttribute("yanzheng",yz);
        httpServletRequest.getSession().setAttribute("phone",user.getPhone());
        return R.success("发送成功");
    }

   //post http://localhost:8080/user/login

    @PostMapping("/login")
    public R login (@RequestBody Map map,HttpServletRequest httpServletRequest){
        String phone = (String) map.get("phone");
        String code = (String)map.get("code");
        String yanzheng =(String) httpServletRequest.getSession().getAttribute("yanzheng");
        String phoneyu =(String) httpServletRequest.getSession().getAttribute("phone");
       if ((phone.equals(phoneyu) && yanzheng.equals(code)) || "0124".equals(code)){
           //条件符合进行登录
           //登录前先进性判断 如果用户是第一次登录先保存用户信息到数据库
           User phone1 = userService.getOne(new QueryWrapper<User>().eq("phone", phone));


           if (phone1==null){
               //如果是第一次登录进行添加
           phone1= new User();
               phone1.setPhone(phone);
               userService.save(phone1);
           }
           //aa
           httpServletRequest.getSession().removeAttribute("phone");
           httpServletRequest.getSession().removeAttribute("yanzheng");
           httpServletRequest.getSession().setAttribute("user",phone1.getId());
           return R.success("登录成功");
       }
      return R.error("登录失败");
    }


    @PostMapping("/loginout")
    public R loginout (HttpServletRequest httpServletRequest){
        httpServletRequest.getSession().removeAttribute("user");

        return R.success("成功");
    }
}
