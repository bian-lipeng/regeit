package com.itheima.regeit_blp.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.regeit_blp.common.R;
import com.itheima.regeit_blp.dto.DishDto;
import com.itheima.regeit_blp.dto.SetmealDto;
import com.itheima.regeit_blp.pojo.*;
import com.itheima.regeit_blp.service.CategoryService;
import com.itheima.regeit_blp.service.DishService;
import com.itheima.regeit_blp.service.SetmealDishService;
import com.itheima.regeit_blp.service.SetmealService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {
    @Autowired
    private DishService dishService;

    @Autowired
    private SetmealService service;

@Autowired
private SetmealDishService setmealDishService;

    @Autowired
    private CategoryService categoryService;

    //GET http://ocalhost:8080/setmeal/page?page=1&pageSize=10&name=%E9%98%BF%E8%90%A8
    // 分页条件chaxun
    @GetMapping("/page")
    public R page(Integer page, Integer pageSize, String name) {

        QueryWrapper<Setmeal> query = new QueryWrapper<>();
        Page<Setmeal> setmealPage = new Page<>(page, pageSize);
        if (name != null) query.like("name", name);
        service.page(setmealPage, query);
        List<Setmeal> records = setmealPage.getRecords();
        List<Setmeal> setmeals = new ArrayList<>();

        for (Setmeal record : records) {

            SetmealDto setmealDto = new SetmealDto();
            BeanUtils.copyProperties(record, setmealDto);
            Category byId = categoryService.getById(record.getCategoryId());
              setmealDto.setCategoryName( byId.getName());
            setmeals.add(setmealDto);
        }
        setmealPage.setRecords(setmeals);
        return R.success(setmealPage);
    }


    //添加数据
    //post http://localhost:8080/setmeal
   @PostMapping
   @CacheEvict(value = "SetmealCache",allEntries = true)
    public R addsetmeal(@RequestBody SetmealDto setmealDto){
             service.addsetmeal(setmealDto);

        return R.success("添加成功");
    }


    //批量删除
    //http://localhost:8080/setmeal?ids=1415580119015145474,1456182156400336897
    //delete

    @DeleteMapping
    @CacheEvict(value = "SetmealCache",allEntries = true)
    public R delete(Long ...ids)  {
        if (ids.length==0){
            return R.error("您未选中商品");
        }
        R r= service.delete(ids);
       return r;
    }


    //post  http://localhost:8080/setmeal/status/1?ids=1456222875852754945

   //批量停售
    @PostMapping("/status/{status}")
    @CacheEvict(value = "SetmealCache",allEntries = true)
    public R update(@PathVariable("status")Integer status, Long...ids){
        for (Long id : ids) {
            Setmeal byId = service.getById(id);
            byId.setStatus(status);
            service.updateById(byId);
        }

   return R.success("状态信息更改成功");
    }

   //get http://localhost:8080/setmeal/1456222875852754945

   //根据套餐id回显套餐数据
   @GetMapping("/{id}")
   public R display(@PathVariable  Long id){
       Setmeal byId = service.getById(id);
       QueryWrapper<SetmealDish> dishFlavorQueryWrapper = new QueryWrapper<>();
       dishFlavorQueryWrapper.eq("setmeal_id",id);
       List<SetmealDish> list = setmealDishService.list(dishFlavorQueryWrapper);

       SetmealDto setmealDto = new SetmealDto();
       BeanUtils.copyProperties(byId,setmealDto);
       setmealDto.setSetmealDishes(list);
       return R.success(setmealDto);

   }

   //修改套餐
   // http://localhost:8080/setmeal

    @PutMapping
    @CacheEvict(value = "SetmealCache",allEntries = true)
    public R updateAll(@RequestBody SetmealDto setmealDto){

        service.updateAll(setmealDto);
        return R.success("修改成功");
    }
     //开启缓存 此注解表示 缓存中有数据直接`返回缓存 没有数据把方法的返回值加入到缓存
     @Cacheable(value = "SetmealCache" ,key="#categoryId+#status")
    @GetMapping("/list")
    public R list(Long categoryId,Integer status){
        QueryWrapper<Setmeal> setmealQueryWrapper = new QueryWrapper<>();
        setmealQueryWrapper.eq("category_id",categoryId).eq("status",status);
        List<Setmeal> list = service.list(setmealQueryWrapper);
        return R.success(list);

    }
    //.点击套餐获取图片
    @GetMapping("/dish/{id}")
    public R dish(@PathVariable Long id){
        QueryWrapper<SetmealDish> setmealDishQueryWrapper = new QueryWrapper<>();
        QueryWrapper<SetmealDish> setmeal_id = setmealDishQueryWrapper.eq("setmeal_id", id);
        List<SetmealDish> list = setmealDishService.list(setmeal_id);
        ArrayList<Dish> dishes = new ArrayList<>();
        for (SetmealDish setmealDish : list) {
            Dish byId = dishService.getById(setmealDish.getDishId());
            dishes.add(byId);
        }

        return  R.success(dishes);
    }
}
