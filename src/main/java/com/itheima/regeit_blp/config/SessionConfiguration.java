package com.itheima.regeit_blp.config;

import com.itheima.regeit_blp.Interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//当前类为配置类
@Configuration
public class SessionConfiguration implements WebMvcConfigurer {
    //重写添加拦截路径方法
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //添加拦截器 实参需要拦截器对象
        InterceptorRegistration interceptor = registry.addInterceptor(new LoginInterceptor());
       //拦截所有
        interceptor.addPathPatterns("/**");
        //排除登录页面

    }
}
