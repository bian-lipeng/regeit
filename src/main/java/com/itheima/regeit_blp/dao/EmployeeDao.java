package com.itheima.regeit_blp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.regeit_blp.pojo.Employee;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmployeeDao extends BaseMapper<Employee> {
}
