package com.itheima.regeit_blp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.regeit_blp.pojo.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao extends BaseMapper<User> {
}
