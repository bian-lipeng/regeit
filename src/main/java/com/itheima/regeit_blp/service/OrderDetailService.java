package com.itheima.regeit_blp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.regeit_blp.pojo.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {
}
