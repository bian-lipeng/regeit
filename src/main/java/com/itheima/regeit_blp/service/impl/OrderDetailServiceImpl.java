package com.itheima.regeit_blp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.regeit_blp.dao.OrderDetailDao;
import com.itheima.regeit_blp.pojo.OrderDetail;
import com.itheima.regeit_blp.service.OrderDetailService;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailDao, OrderDetail> implements OrderDetailService {
}
