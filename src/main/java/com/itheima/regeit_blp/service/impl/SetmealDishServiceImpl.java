package com.itheima.regeit_blp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.regeit_blp.dao.SetmealDao;
import com.itheima.regeit_blp.dao.SetmealDishDao;
import com.itheima.regeit_blp.pojo.SetmealDish;
import com.itheima.regeit_blp.service.SetmealDishService;
import org.springframework.stereotype.Service;

@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishDao, SetmealDish> implements SetmealDishService {
}
