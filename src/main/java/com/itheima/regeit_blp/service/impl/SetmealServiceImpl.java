package com.itheima.regeit_blp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.regeit_blp.common.R;
import com.itheima.regeit_blp.dao.SetmealDao;
import com.itheima.regeit_blp.dto.SetmealDto;
import com.itheima.regeit_blp.pojo.Setmeal;
import com.itheima.regeit_blp.pojo.SetmealDish;
import com.itheima.regeit_blp.service.SetmealDishService;
import com.itheima.regeit_blp.service.SetmealService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;

import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;



import java.util.List;

@Service
public class SetmealServiceImpl   extends ServiceImpl<SetmealDao, Setmeal> implements SetmealService {

    @Autowired
    private SetmealDishService setmealDishService;
    //添加方法

    //手动控制事务
    @Autowired
    DataSourceTransactionManager dataSourceTransactionManager;
    @Autowired
    TransactionDefinition transactionDefinition;


    @Transactional  //开启事务
    @Override
    public void addsetmeal(SetmealDto setmealDto) {
        //数据分发
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDto,setmeal);
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        save(setmeal);
        for (SetmealDish setmealDish : setmealDishes) {
            setmealDish.setSetmealId(setmeal.getId());
            setmealDishService.save(setmealDish);
        }

    }
       //开启事务
    @Override
    public R delete(Long[] ids){

        //此方法开启事务
        TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
        //操作两张表开启事务
        for (Long id : ids) {
            Integer status = getById(id).getStatus();
            if (status==1){
            //遇见状态等于1 表示在售停止删除回滚事务
                //
                dataSourceTransactionManager.rollback(transactionStatus);
                return R.error("选中套餐中有在售套餐");

            }
            removeById(id);
            setmealDishService.remove(new QueryWrapper<SetmealDish>().eq("setmeal_id",id));

        }
        //提交事务
        dataSourceTransactionManager.commit(transactionStatus);
        return R.success("删除成功");

    }

    //修改数据
    //数据回显
    @Transactional //开启事务
    @Override
    public void updateAll(SetmealDto setmealDto) {
        //数据分发
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDto,setmeal);
        updateById(setmeal);
        //对套餐信息进行单独更改
        //接下来对菜品信息进行更改
        //先删除原有表单数据再进行添加
        setmealDishService.remove(new QueryWrapper<SetmealDish>().eq("setmeal_id",setmeal.getId()));
        //移除完成进行二次添加
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        for (SetmealDish setmealDish : setmealDishes) {
           setmealDish.setSetmealId(setmeal.getId());
            setmealDishService.save(setmealDish);
        }

    }
}
