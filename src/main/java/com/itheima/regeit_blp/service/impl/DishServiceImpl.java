package com.itheima.regeit_blp.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.regeit_blp.common.R;
import com.itheima.regeit_blp.dao.DishDao;
import com.itheima.regeit_blp.dto.DishDto;
import com.itheima.regeit_blp.pojo.Dish;
import com.itheima.regeit_blp.pojo.DishFlavor;
import com.itheima.regeit_blp.pojo.SetmealDish;
import com.itheima.regeit_blp.service.DishFlavorService;
import com.itheima.regeit_blp.service.DishService;
import com.itheima.regeit_blp.service.SetmealDishService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DishServiceImpl extends ServiceImpl<DishDao, Dish> implements DishService {
    @Autowired
    private  DishService dishService;

    @Autowired
    private DishFlavorService dishFlavorService;

@Autowired
private SetmealDishService setmealDishService;

    //手动控制事务
    @Autowired
    DataSourceTransactionManager dataSourceTransactionManager;
    @Autowired
    TransactionDefinition transactionDefinition;

    @Override
    //为此方法开启事务
    @Transactional
    public void adddishdto(DishDto dishDto) {
     //对dishdto信息进行拆解 分别进行存储
        DishFlavor dishFlavor = new DishFlavor();
        Dish dish = new Dish();
       BeanUtils.copyProperties(dishDto,dish);
         dishService.save(dish);

        BeanUtils.copyProperties(dishDto,dish);List<DishFlavor> flavors = dishDto.getFlavors();
        for (DishFlavor flavor : flavors) {
            BeanUtils.copyProperties(flavor,dishFlavor);
            dishFlavor.setDishId(dish.getId());

            dishFlavorService.save(dishFlavor);
        }
      }
      @Transactional
    @Override
    public R updateAll(DishDto dishDto) {
          Dish dish = new Dish();
          DishFlavor dishFlavor = new DishFlavor();
          BeanUtils.copyProperties(dishDto,dish);
          dishFlavorService.remove(new QueryWrapper<DishFlavor>().eq("dish_id",dish.getId()));
          dishService.updateById(dish);
          List<DishFlavor> flavors = dishDto.getFlavors();
          for (DishFlavor flavor : flavors) {
              BeanUtils.copyProperties(flavor,dishFlavor);
              flavor.setDishId(dish.getId());
              dishFlavorService.save(flavor);
          }

          return R.success("修改成功");
    }


    //批量删除菜品删除
    @Override
    public R delete(Long[] ids) {
        //此方法开启事务
        TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
        for (Long id : ids) {
            Dish byId = dishService.getById(id);
            //判断售卖信息,停售则可以删除
            if (byId.getStatus()==0){
                //二重判断 判断该菜品在不在某一个套餐中 在则不允许删除
                String name = byId.getName();
                List<SetmealDish> list = setmealDishService.list(new QueryWrapper<SetmealDish>().eq("name", name));
                if (list.isEmpty()){
                    dishService.removeById(id);
                }else{
                    //
                    dataSourceTransactionManager.rollback(transactionStatus);
                    return R.error("套餐中存在改菜品需要先删除套餐");

                }


            }else{
                //
                dataSourceTransactionManager.rollback(transactionStatus);
                return R.error("选中菜品中有在售菜品");

            }

        }

        //提交事务
        dataSourceTransactionManager.commit(transactionStatus);
        return R.success("删除成功");
    }


}
