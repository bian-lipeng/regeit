package com.itheima.regeit_blp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.regeit_blp.dao.EmployeeDao;
import com.itheima.regeit_blp.pojo.Employee;
import com.itheima.regeit_blp.service.EmployeeService;
import org.springframework.stereotype.Service;
//实现接口 继承ServiceImpl方法
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeDao, Employee> implements EmployeeService {
}
