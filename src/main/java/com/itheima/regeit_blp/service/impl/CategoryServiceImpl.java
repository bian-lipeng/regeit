package com.itheima.regeit_blp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.regeit_blp.dao.CategoryDao;
import com.itheima.regeit_blp.pojo.Category;
import com.itheima.regeit_blp.service.CategoryService;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryDao,Category> implements CategoryService {
}
