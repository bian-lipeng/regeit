package com.itheima.regeit_blp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.regeit_blp.dao.ShoppingCartDao;
import com.itheima.regeit_blp.pojo.ShoppingCart;
import com.itheima.regeit_blp.service.ShoppingCartService;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartDao, ShoppingCart> implements ShoppingCartService {
}