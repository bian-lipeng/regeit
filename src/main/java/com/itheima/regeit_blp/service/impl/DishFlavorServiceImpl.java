package com.itheima.regeit_blp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.regeit_blp.common.R;
import com.itheima.regeit_blp.dao.DishFlavorDao;
import com.itheima.regeit_blp.dto.DishDto;
import com.itheima.regeit_blp.pojo.DishFlavor;
import com.itheima.regeit_blp.service.DishFlavorService;
import org.springframework.stereotype.Service;

@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorDao, DishFlavor> implements DishFlavorService {


}
