package com.itheima.regeit_blp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.regeit_blp.common.R;
import com.itheima.regeit_blp.dto.DishDto;
import com.itheima.regeit_blp.pojo.Dish;
import com.itheima.regeit_blp.pojo.Employee;

public interface DishService extends IService<Dish> {

     void adddishdto(DishDto dishDto);

    R updateAll(DishDto dishDto);

    R delete(Long[] ids);
}
