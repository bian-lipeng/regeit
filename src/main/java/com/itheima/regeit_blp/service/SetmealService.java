package com.itheima.regeit_blp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.regeit_blp.common.R;
import com.itheima.regeit_blp.dto.SetmealDto;
import com.itheima.regeit_blp.pojo.Setmeal;

import java.sql.SQLClientInfoException;
import java.sql.SQLException;

public interface SetmealService extends IService<Setmeal> {
    void addsetmeal(SetmealDto setmealDto);

    R delete(Long[] ids) ;

    void updateAll(SetmealDto setmealDto);
}
