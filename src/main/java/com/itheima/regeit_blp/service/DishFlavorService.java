package com.itheima.regeit_blp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.regeit_blp.pojo.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}
