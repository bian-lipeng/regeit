package com.itheima.regeit_blp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.regeit_blp.pojo.Orders;

public interface OrdersService extends IService<Orders> {
    void add(Orders orders);
}
