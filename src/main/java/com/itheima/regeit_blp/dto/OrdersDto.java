package com.itheima.regeit_blp.dto;


import com.itheima.regeit_blp.pojo.OrderDetail;
import com.itheima.regeit_blp.pojo.Orders;
import lombok.Data;
import java.util.List;

@Data
public class OrdersDto extends Orders {

    private String userName;

    private String phone;

    private String address;

    private String consignee;

    private List<OrderDetail> orderDetails;
	
}
