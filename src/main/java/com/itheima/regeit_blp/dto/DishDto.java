package com.itheima.regeit_blp.dto;


import com.itheima.regeit_blp.pojo.Dish;
import com.itheima.regeit_blp.pojo.DishFlavor;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class DishDto extends Dish {

    private List<DishFlavor> flavors = new ArrayList<>();

    private String categoryName;

    private Integer copies;
}
