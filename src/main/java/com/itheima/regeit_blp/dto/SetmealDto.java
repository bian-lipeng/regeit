package com.itheima.regeit_blp.dto;

import com.itheima.regeit_blp.pojo.Setmeal;
import com.itheima.regeit_blp.pojo.SetmealDish;

import lombok.Data;
import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
