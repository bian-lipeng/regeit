package com.itheima.regeit_blp.common;

public class BaseContext {
    private static ThreadLocal<Long> threadLocal=new ThreadLocal<>();

    //设置线程集合类存储的元素
  public static  void setTreadLocal(Long id){
      threadLocal.set(id);

  }

public static  Long getTreadLocal(){

      return threadLocal.get();
}

}
