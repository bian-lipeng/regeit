package com.itheima.regeit_blp.common;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

//字段自动插入
//插入策略
@Component
public class MyMetaObjecthandler implements MetaObjectHandler {
@Autowired
private HttpServletRequest httpServletRequest;
    @Override
 public void insertFill(MetaObject metaObject) {
   //插入时候添加数据 方案一自动注入session从session中获取
                   //二 调用threadlocal方法存储获取该方法可以在单线程中实现数据的存储
      //  Long employee = (Long)httpServletRequest.getSession().getAttribute("employee");
        Long treadLocal = BaseContext.getTreadLocal();
        metaObject.setValue("createUser",treadLocal);
        metaObject.setValue("createTime",LocalDateTime.now());
        metaObject.setValue("updateTime",LocalDateTime.now());
        metaObject.setValue("updateUser",treadLocal);

    }
    @Override
    public void updateFill(MetaObject metaObject) {

        Long employee = BaseContext.getTreadLocal();
     //   Long employee = (Long)httpServletRequest.getSession().getAttribute("employee");
        metaObject.setValue("updateTime",LocalDateTime.now());
        metaObject.setValue("updateUser",employee);


    }
}
